using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleController : MonoBehaviour
{
    public float destroyAfter = 2;
    private bool letDestroy = true;

    // Start is called before the first frame update
    void Start()
    {
        float r = UnityEngine.Random.Range(0, 1f);
        float g = UnityEngine.Random.Range(0, 1f);
        float b = UnityEngine.Random.Range(0, 1f);

        float s = UnityEngine.Random.Range(1f, 3f);

        GetComponent<SpriteRenderer>().color = new Color(r, g, b, 1);

        transform.localScale = Vector3.one * s;

        //if (r==0)
        //{
        //    GetComponent<SpriteRenderer>().color = Color.green;
        //}
        //else if (r==1)
        //{
        //    GetComponent<SpriteRenderer>().color = Color.red;
        //}
        //else
        //{
        //    GetComponent<SpriteRenderer>().color = Color.blue;
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (!letDestroy)
        {
            return;
        }

        destroyAfter = destroyAfter - Time.deltaTime;

        if (destroyAfter <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void StopDestroy()
    {
        letDestroy = false;
    }
}
