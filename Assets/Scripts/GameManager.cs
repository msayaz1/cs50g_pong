using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject MyCircle;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Started!");   
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {

            GameObject item = Instantiate(MyCircle);
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            item.transform.position = pos;

        }

    }
}
